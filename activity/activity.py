
year = input('Enter year:\n')

year = int(year)
if (year <= 0):
    print(f"{year} is negative or zero")
else:
    if year != 0 and year % 4 == 0:
        print(f"{year} is a leap year")
    else:
        print(f"{year} is a not leap year")

rows = int(input('Enter number of rows:\n'))
cols = int(input('Enter number of colums:\n'))

for x in range(rows):
    for y in range(cols):
        print("*", end='')
    print()
    


